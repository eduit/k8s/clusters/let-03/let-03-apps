#!/bin/bash

mkdir -p ./amt-service ./ansible-job ./frontend ./shared

# Create a variable
PATH_TO_BASE="../../../../bases/k8s-apps-base/exam-portal"

kubectl create secret generic --from-literal=POSTGRES_HOST=eduit-exam-portal-db-dev.ethz.ch --dry-run=client exam-portal-postgres-host -o yaml >postgres-service/private_patch-postgres-hostname.yml

kubeseal --kubeconfig ~/.kube/let-03 <postgres-service/private_patch-postgres-hostname.yml -n exam-portal --scope=namespace-wide >postgres-service/patch-postgres-hostname.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/amt-service/private_amt_credentials.yml -n exam-portal --scope=namespace-wide >amt-service/amt_credentials.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/amt-service/private_intel_amt_certificate.yml -n exam-portal  --scope=namespace-wide >amt-service/intel_amt_certificate.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/ansible-job/private_ansible_deployment_config.yml -n exam-portal  --scope=namespace-wide >ansible-job/ansible_deployment_config.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/ansible-job/private_ansible_paths.yml -n exam-portal  --scope=namespace-wide >ansible-job/ansible_paths.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/ansible-job/private_vault_file_password.yml -n exam-portal  --scope=namespace-wide >ansible-job/vault_file_password.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/shared/private_gitlab_ansible_token.yml -n exam-portal --scope=namespace-wide >shared/gitlab_ansible_token.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/shared/private_nats_credentials.yml -n exam-portal --scope=namespace-wide >shared/nats_credentials.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/shared/private_postgres_credentials.yml -n exam-portal --scope=namespace-wide >shared/postgres_credentials.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/shared/private_redis_credentials.yml -n exam-portal --scope=namespace-wide >shared/redis_credential.json

kubeseal --kubeconfig ~/.kube/let-03 <$PATH_TO_BASE/shared/private_registry_credentials.yml -n exam-portal --scope=namespace-wide >shared/registry_credentials.json