#!/usr/bin/bash

NAME=$1
TOKEN=$2

if [ -z "${NAME}" ] || [ -z "${TOKEN}" ];  then
    echo "Usage: $0 NAME TOKEN; NAME is the suffix for the secret name after 'gitlab-runner-'"
    exit
fi

kubectl create secret generic gitlab-runner-$NAME --from-literal runner-token="$TOKEN" --from-literal runner-registration-token="" --dry-run=client -o json >private_gitlab-runner-$NAME.json
kubeseal <private_gitlab-runner-$NAME.json -n gitlab-runner-$NAME --namespace=gitlab-runner --scope=namespace-wide >gitlab-runner-$NAME.json

rm private_gitlab-runner-$NAME.json
